//
//  OddOccurrencesInArray.m
//  Codility1
//
//  Created by Felipe Polidori Rios on 19/04/16.
//  Copyright © 2016 Felipe Polidori Rios. All rights reserved.
//

#import "OddOccurrencesInArray.h"

@implementation OddOccurrencesInArray

-(int)solution:(NSMutableArray *)A {
    
    if (A.count == 1) {
        return [(NSNumber *)A[0] intValue];
    }
    
    NSNumber *valuee = A[0];
    BOOL resultFound = NO;
    BOOL isFirstTime = YES;
    
    while (resultFound == NO || isFirstTime) {
        isFirstTime = NO;
        resultFound = YES;
        for (int j = 1; j < A.count; j++) {
            NSNumber *value = A[j];
            if (value.intValue == valuee.intValue) {
                resultFound = NO;
                [A removeObjectsInArray:@[valuee,value]];
                valuee = A[0];
                break;
            }
        }
        
    }
    return valuee.intValue;
}

@end
