//
//  AppDelegate.h
//  Codility1
//
//  Created by Felipe Polidori Rios on 18/04/16.
//  Copyright © 2016 Felipe Polidori Rios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

