//
//  CyclicRotationViewController.m
//  Codility1
//
//  Created by Felipe Polidori Rios on 19/04/16.
//  Copyright © 2016 Felipe Polidori Rios. All rights reserved.
//

#import "CyclicRotationViewController.h"

@implementation CyclicRotationViewController

-(void)viewDidLoad {
    [super viewDidLoad];
}

//problem:
//https://codility.com/programmers/task/cyclic_rotation/

- (NSArray *)rotateArray:(NSArray *)array kTimes:(int)K {
    
    NSLog(@"Array to be rotated %i times: %@", K, array);
    
    if (array.count == 0) {
        return array;
    }
    
    NSMutableArray *mutableArray = [[NSMutableArray alloc]initWithArray:array];
    int i = 0;
    while (i < K) {
        NSNumber *lastNumber = mutableArray.lastObject;
        [mutableArray removeObjectAtIndex:mutableArray.count-1];
        [mutableArray insertObject:lastNumber atIndex:0];
        i++;
    }
    
    NSLog(@"Array rotated: %@", mutableArray);
    return mutableArray;
}


@end
