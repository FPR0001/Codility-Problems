//
//  main.m
//  Codility1
//
//  Created by Felipe Polidori Rios on 18/04/16.
//  Copyright © 2016 Felipe Polidori Rios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
