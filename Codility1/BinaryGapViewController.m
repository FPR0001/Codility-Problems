//
//  BinaryGapViewController.m
//  Codility1
//
//  Created by Felipe Polidori Rios on 19/04/16.
//  Copyright © 2016 Felipe Polidori Rios. All rights reserved.
//

#import "BinaryGapViewController.h"

@implementation BinaryGapViewController

-(void)viewDidLoad {
    [super viewDidLoad];
    [self solvesTheProblem];
    [self solvesTheProblemInAnotherWay];
}

/*
 Problem:
 https://codility.com/programmers/task/binary_gap/
 */

- (void)solvesTheProblem {
    
    int N = 13579;
    NSLog(@"Number N to be used for this problem: %i", N);
    NSLog(@"Binary representation of N: %@", N == 0 ? @"0" : [self getBinaryNumberInString:N withInitialString:@""]);
    NSDictionary *dicResponse = [self searchForLongestBinaryGap:N dic:@{ @"s" : @(0), @"m" : @(0)}];
    NSLog(@"Length of the longest binary gap found in N: %@", N == 0 ? @"0" : dicResponse[@"m"]);
}


/*
 Function searches for the longest binary gap in the int N;
 To use this function, pass as the dic -> @{ @"s" : @(0), @"m" : @(0)};
 Function returns a dictionary with the length of the longest binary gap in the key 'm';
 */

- (NSDictionary *)searchForLongestBinaryGap:(int)N dic:(NSDictionary *)dic{
    
    int divisionResult = N / 2;
    int remaining = N % 2;
    
    if (divisionResult > 1) {
        NSDictionary *dicAux = [self searchForLongestBinaryGap:divisionResult dic:dic];
        NSNumber *bridgeSum = dicAux[@"s"];
        NSNumber *bridgeMax = dicAux[@"m"];
        if (remaining == 1) {
            bridgeSum = @(0);
        } else {
            bridgeSum = @(bridgeSum.intValue + 1);
            if (bridgeSum > bridgeMax) {
                bridgeMax = bridgeSum;
            }
        }
        return @{ @"s" : bridgeSum, @"m" : bridgeMax };
    } else {
        NSNumber *n = divisionResult > 0 ? @(0) : @(1);
        return @{ @"s" : n, @"m" : n };
    }
}


/*
 Function returns the string binary representation of the int N;
 To use this function, pass as the initialString an empty string.
 */
- (NSString *)getBinaryNumberInString:(int)N withInitialString:(NSString *)initialString{
    
    int divisionResult = N / 2;
    int remaining = N % 2;
    
    if (divisionResult > 1) {
        NSString *stringToAppend = [self getBinaryNumberInString:divisionResult withInitialString:initialString];
        return [NSString stringWithFormat:@"%@%i", stringToAppend, remaining];
    } else {
        return [NSString stringWithFormat:@"%@%i", initialString, divisionResult];
    }
}



//****************************************************************************

- (void)solvesTheProblemInAnotherWay {
    
    int N = 13579;
    
    NSLog(@"Number N to be used for this problem: %i", N);

    int numberDivisionsByTwo = [self countNumberOfDivisionsByTwo:N];
    NSMutableArray *a = [[NSMutableArray alloc]init];
    int sumNumbers = 0;
    int sumBridge = 0;
    int maxSumBridge = 0;
    for (int i = numberDivisionsByTwo; i>-1; i--) {
        int twoInThePowerOfi = pow(2, i);
        sumNumbers = sumNumbers + twoInThePowerOfi;
        if (sumNumbers == N) {
            [a addObject:[NSNumber numberWithBool:YES]];
            if (sumBridge > maxSumBridge) {
                maxSumBridge = sumBridge;
            }
            sumBridge = 0;
            //we found the binary number here
            break;
        } else if (sumNumbers > N) {
            sumNumbers = sumNumbers - twoInThePowerOfi;
            [a addObject:[NSNumber numberWithBool:NO]];
            sumBridge++;
        } else {
            [a addObject:[NSNumber numberWithBool:YES]];
            if (sumBridge > maxSumBridge) {
                maxSumBridge = sumBridge;
            }
            sumBridge = 0;
        }
    }
    
    NSLog(@"Binary representation of N: %@", N == 0 ? @"0" : [a componentsJoinedByString:@""]);
    NSLog(@"Length of the longest binary gap found in N: %i", N == 0 ? 0 : maxSumBridge);
}

- (int)countNumberOfDivisionsByTwo:(int)number {
    int result = number > 1 ? 1 : 0;
    int divisionResult = number / 2;
    while (divisionResult > 1) {
        result ++;
        divisionResult = divisionResult / 2;
    }
    return result;
}

@end
